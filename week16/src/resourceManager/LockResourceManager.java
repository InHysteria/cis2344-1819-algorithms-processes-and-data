package resourceManager;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An implementation of BasicResourceManager, utilising Locks and Conditions to control access to a Resource with a priority system.
 *
 * @author Gemma Mallinson
 * @version February 2018
 */
public class LockResourceManager extends BasicResourceManager
{
	private final Lock _lock = new ReentrantLock();	//The lock which allows for atomic access to this objects methods.
	private final Condition[] _queues = new Condition[NO_OF_PRIORITIES]; //An array of conditions representing differing priorities to await.
	private boolean _inUse = false; //Is the resource currently in use, if false then no awaiting is necessary.

    /**
     * The constructor initialises the various conditions for each priority, sets the resource to be used and how many uses can be provided in total.
     * @param resource The resource to control access too.
     * @param maxUses How many uses in total can be provided.
     */            
	public LockResourceManager(Resource resource, int maxUses) 
	{
		super(resource, maxUses);
		
		for (int i = 0; i < NO_OF_PRIORITIES; i++)	//For each priority
			_queues[i] = _lock.newCondition();		//Create  a new condition to act as a "Queue"
	}

	/**
	 * Requests access to the resource with the given priority.
	 * If the resource is in use then the thread will sleep until it is the highest priority request and another thread has released the resource. 
	 * Otherwise the resource is provided.
	 * 
	 * @param priority The priority of this request from 0 (inclusive) to NO_OF_PRIORITIES (exclusive), higher priorities are served first.
	 */
	@Override
	public void requestResource(int priority) throws ResourceError
	{
		_lock.lock(); //Lock this object so that no other threads may enter until the current thread has returned or is awaiting.
		try 
		{
			if (_inUse) //If the resource is currently in use.
			{
				increaseNumberWaiting(priority); //Increase the number of threads waiting at <priority>
				_queues[priority].await();	//Wait until signaled.
				decreaseNumberWaiting(priority); //Reduce the number of threads waiting at <priority> as this thread has just finished waiting.
			}
			
			_inUse = true; //Mark that this thread is using the resource.
		}
		catch (InterruptedException ex) 
		{
			System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
		}
		finally
		{
			_lock.unlock(); //Always unlock the method when execution leaves the try block, be it from exception or by simply leaving. 
		}
	}


	/**
	 * Release access to the resource after it has been used.
	 * If any other threads are waiting the one with the highest priority is signaled. 
	 * 
	 * @return The priority of thread that was signaled or NONE_WAITING if there are no other threads waiting.
	 */
	@Override
	public int releaseResource() throws ResourceError 
	{
		_lock.lock(); //Lock this object so that no other threads may enter until the current thread has returned or is awaiting.
		try 
		{
			_inUse = false;	//Mark that the resource is no longer in use, this is safe as this method is atomic due to _lock.lock()
			
			/*
			Alternative syntax. 
			 
			int priority = NO_OF_PRIORITIES-1;
			for (; 
				priority >= 0 && getNumberWaiting(priority) <= 0;
				priority--);
			*/
				
			int priority = NO_OF_PRIORITIES-1; //Start at the highest priority.		
			while (priority >= 0 && getNumberWaiting(priority) <= 0) //While the current priority is 0 or greater, and there are no resources waiting at this priority.
				priority--; //Deincrement the priority to consider the next highest.
			
			if (priority < 0) //If priority is below 0 then no thread was waiting.
				return NONE_WAITING;
			
			_queues[priority].signal(); //Signal a thread of the highest priority with threads waiting.
			return priority; //Then return that priority.
		}
		finally
		{
			_lock.unlock(); //Always unlock the method when execution leaves the try block, be it from exception or by returning. 
		}
	}
}
