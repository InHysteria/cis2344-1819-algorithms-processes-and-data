package graph;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.junit.jupiter.api.Test;

public class TopologicalSortImplementationTest
{
	@Test
	public void TestValidSimple3() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);

		graph.add(0,1);
		graph.add(0,2);
		
		/*
		 * 0 -> 1
		 * |
		 * V
		 * 2
		 */

		testIsTopologicalSort(graph.getSort(), graph);
	}
	@Test
	public void TestValidSimple5() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);
		graph.add(3);
		graph.add(4);

		graph.add(0,1);
		graph.add(0,3);
		graph.add(1,2);
		graph.add(2,4);
		
		/*
		 * 0 -> 1 -> 2
		 * |		 |
		 * V		 V
		 * 3		 4
		 */

		testIsTopologicalSort(graph.getSort(), graph);
	}
	@Test
	public void TestValidSimple10() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);
		graph.add(3);
		graph.add(4);
		graph.add(5);
		graph.add(6);
		graph.add(7);
		graph.add(8);
		graph.add(9);


		graph.add(0,1);
		graph.add(0,3);
		graph.add(1,2);
		graph.add(2,4);
		graph.add(4,5);
		graph.add(5,6);
		graph.add(9,0);
		graph.add(9,8);
		graph.add(8,7);
		
		/*
		 * 9 -> 0 -> 1 -> 2
		 * |	|		  |
		 * V	V		  V
		 * 8	3		  4
		 * |			  |
		 * V			  V
		 * 7			  5
		 * 				  |
		 * 				  V
		 * 				  6
		 */

		testIsTopologicalSort(graph.getSort(), graph);
	}
	@Test
	public void TestValidComplex3() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);

		graph.add(0,1);
		graph.add(0,2);
		graph.add(1,2);
		
		/*
		 * 0 -> 1
		 * |    |
		 * V    |
		 * 2<---+   
		 */

		testIsTopologicalSort(graph.getSort(), graph);
	}
	@Test
	public void TestValidComplex5() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);
		graph.add(3);
		graph.add(4);

		graph.add(0,1);
		graph.add(0,3);
		graph.add(1,2);
		graph.add(2,4);
		graph.add(1,3);
		graph.add(4,3);
		
		/*
		 * 0 -> 1 -> 2
		 * |	|	 |
		 * V	|	 V
		 * 3<---+----4
		 */

		testIsTopologicalSort(graph.getSort(), graph);
	}
	@Test
	public void TestValidComplex10() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);
		graph.add(3);
		graph.add(4);
		graph.add(5);
		graph.add(6);
		graph.add(7);
		graph.add(8);
		graph.add(9);


		graph.add(0,1);
		graph.add(0,3);
		graph.add(1,2);
		graph.add(2,4);
		graph.add(4,5);
		graph.add(5,6);
		graph.add(9,0);
		graph.add(9,8);
		graph.add(8,7);

		graph.add(3,8);
		graph.add(3,1);
		graph.add(3,4);
		
		/*
		 * 9 -> 0 -> 1 -> 2
		 * |	|	 ^	  |
		 * V	V	 |	  V
		 * 8 <- 3 ---+--> 4
		 * |			  |
		 * V			  V
		 * 7			  5
		 * 				  |
		 * 				  V
		 * 				  6
		 */
		
		testIsTopologicalSort(graph.getSort(), graph);
	}

	@Test
	public void TestInvalid() throws GraphError
	{
		TopologicalSortImplementation<Integer> graph = new TopologicalSortImplementation<Integer>();

		graph.add(0);
		graph.add(1);
		graph.add(2);
		graph.add(3);

		graph.add(0,1);
		graph.add(1,2);
		graph.add(2,3);
		graph.add(3,0);
		
		/*
		 * 0 -> 1
		 * ^    |
		 * |    V
		 * 3 <- 2
		 */

		assertThrows(GraphError.class, () -> testIsTopologicalSort(graph.getSort(), graph));
	}
	
	private void testIsTopologicalSort(List<Integer> sort, AdjacencyGraph<Integer> graph) throws GraphError
	{
		for (Integer node : sort)
		{
			int i = sort.indexOf(node);
			for (Integer neighbour : graph.getNeighbours(node))
				//If any child of this node, appears before this node in the sort then it is invalid.
				if (sort.indexOf(neighbour) < i)
					fail("Sort is invalid, " + neighbour + " must appear after " + node);
		}
	}
}
