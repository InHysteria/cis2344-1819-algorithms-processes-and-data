package graph;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Extends an AjacencyGraph by providing a topological sort solution.
 * 
 * @author Gemma Mallinson
 * @version November 2018
 */
public class TopologicalSortImplementation<T> extends AdjacencyGraph<T> implements TopologicalSort<T> 
{
	/**
	 * Do a topological sort on this graph, if it is acyclic.  A topological sort is a listing of the nodes in
	 * the (acyclic) graph such that if there is a path in the graph from node A to node B then node A will appear
	 * before node B in the listing.
	 * @return a topological sort of this graph
	 * @throws GraphError if the graph is not acyclic
	 */
	public List<T> getSort() throws GraphError
	{
		T current;
		Set<T> todo = getNodes(); 
		Stack<T> output = new Stack<T>();
		Hashtable<T,Integer> predeccessors = new Hashtable<T,Integer>();
		
		for (T node : todo)
			for (T neighbour : getNeighbours(node))
				predeccessors.compute(neighbour, (x,y) -> y == null ? 1 : y + 1);
		
		while (!todo.isEmpty())
		{
			current = null;
			for (T node : todo)
				if (predeccessors.getOrDefault(node, 0) <= 0) //Nodes which have no predeccessors initially should not be in this dictionary.
					current = node;
				
			if (current == null)
				throw new GraphError("This graph is cyclic and thus does not have a topological sort.");

			for (T neighbour : getNeighbours(current))
				predeccessors.compute(neighbour, (x,y) -> y == null ? 0 : y - 1);
			
			output.push(current);
			todo.remove(current);
		}
		
		return output;
	}
}
