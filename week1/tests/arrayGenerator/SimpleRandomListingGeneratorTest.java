package arrayGenerator;

/**
 * @author Hugh Osborne
 * @version September 2018
 */

class SimpleRandomListingGeneratorTest extends ListingGeneratorTest 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)
	
	@Override
	protected ListingGenerator createArrayGenerator(int size) 
	{
		return new SimpleRandomListingGenerator(size);
	}
}