package arrayGenerator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

abstract class ArrayGeneratorTest 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)

    abstract protected ArrayGenerator createArrayGenerator(int size);

    /**
     * Test whether the generator generates arrays of the correct size
     * @param generator the generator to be tested
     */
    private void _testSize(ArrayGenerator generator) 
    {
        assertEquals(generator.GetSize(), generator.GetArray().length);
    }

    @Test
    void TestOneSize() 
    {
        _testSize(createArrayGenerator(1));
    }

    @Test
    void TestTwoSize() 
    {
        _testSize(createArrayGenerator(2));
    }

    @Test
    void TestFourSize() 
    {
        _testSize(createArrayGenerator(4));
    }

    @Test
    void TestHundredSize() 
    {
        _testSize(createArrayGenerator(100));
    }
    
	@Test
	void TestThousandSize() 
	{
        _testSize(createArrayGenerator(1000));
	}
	
	@Test
	void TestMillionSize() 
	{
	    _testSize(createArrayGenerator(1000000));
	}

}