package searcher;

/**
 * @author Hugh Osborne
 * @version September 2018
 */

class SimpleSearcherTest extends SearcherTest 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)

	protected Searcher createSearcher(int[] array, int index) throws IndexingError 
	{
		return new SimpleSearcher(array,index);
	}

}