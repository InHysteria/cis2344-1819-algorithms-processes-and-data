package searcher;

import arrayGenerator.ArrayGenerator;
import arrayGenerator.CleverRandomListingGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Hugh Osborne
 * @version September 2018
 */

abstract class SearcherTest 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)
	/**
	 * Create a searcher of the right type
	 */
	abstract protected Searcher createSearcher(int[] array, int index) throws IndexingError;

	/**
	 * Test that the searcher finds the corretc value.  The test uses a random listing generator to create
	 * a random listing of the requited size.  Because of the properties of random listings, the kth largest
	 * element of a random listing of size n must be n-k.
	 * @param arraySize the size of the random listing to be generated (the "n" value)
	 * @param index the index (the "k" value)
	 * @throws IndexingError if k is out of bounds for n
	 */
	private void _testSearcher(int arraySize,int index) throws IndexingError 
	{
		ArrayGenerator generator = new CleverRandomListingGenerator(arraySize);
		Searcher search = createSearcher(generator.GetArray(), index);
		assertEquals(arraySize - index, search.FindElement());
	}

	@Test
	void Test2ndIn10() throws IndexingError 
	{
		_testSearcher(10,2);
	}

	@Test
	void Test5thIn10() throws IndexingError 
	{
		_testSearcher(10,5);
	}
	@Test
	void Test3rdIn100() throws IndexingError 
	{
		_testSearcher(100,3);
	}

	@Test
	void Test16thIn100() throws IndexingError 
	{
		_testSearcher(100,16);
	}

	@Test
	void Test8thIn1000() throws IndexingError 
	{
		_testSearcher(1000,8);
	}

	@Test
	void Test107thIn1000() throws IndexingError 
	{
		_testSearcher(1000,107);
	}

	@Test
	void Test1stIn10000() throws IndexingError 
	{
		_testSearcher(10000,1);
	}

	@Test
	void Test1003rdn10000() throws IndexingError 
	{
		_testSearcher(10000,1003);
	}

	@Test
	void Test11thIn100000() throws IndexingError 
	{
		_testSearcher(100000,11);
	}

	@Test
	void Test4thIn1000000() throws IndexingError 
	{
		_testSearcher(1000000,4);
	}

}