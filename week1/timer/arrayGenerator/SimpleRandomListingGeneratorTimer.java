package arrayGenerator;

/*
  A timer implementation for simple random listing generators that times the randomise method.

  @author Hugh Osborne
 * @version September 2018
 */

import timer.Timer;

public class SimpleRandomListingGeneratorTimer extends SimpleRandomListingGenerator implements Timer 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)

	private SimpleRandomListingGeneratorTimer(int size)
	{
		super(size);
	}

	@Override
	public Timer GetTimer(int size) 
	{
		return new SimpleRandomListingGeneratorTimer(size);
	}

	@Override
	public void TimedMethod() 
	{
		Randomise();
	}

	@Override
	public long GetMaximumRuntime() 
	{
		return 5;
	}

	@Override
	public int GetMinimumTaskSize() 
	{
		return 1;
	}

	@Override
	public int GetMaximumTaskSize() 
	{
		return 1000000000;
	}

	public static void Main(String[] args) {
		SimpleRandomListingGeneratorTimer timer = new SimpleRandomListingGeneratorTimer(0);
		timer.timingSequence();
	}
}
