package searcher;

/*
  A timer implementation for simple searchers that times the findElement method

  @author Hugh Osborne
 * @version September 2018
 */

import arrayGenerator.ArrayGenerator;
import arrayGenerator.CleverRandomListingGenerator;
import timer.Timer;

public class SimpleSearcherTimer extends SimpleSearcher implements Timer 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)

	// All timings will be done with an index of 5
	private final static int K = 5;

	private SimpleSearcherTimer(int[] array) 
	{
		super(array, K);
	}

	@Override
	public void TimedMethod() 
	{
		try 
		{
			FindElement();
		} 
		catch (IndexingError indexingError) 
		{
			// simply ignore indexing errors here
			// with K at 5, and a minimum task size (array size) of 10, indexing errors should not occur
			// duirng timing
		}
	}

	@Override
	public long GetMaximumRuntime() 
	{
		return 1;
	}

	/**
	 * Minimum task size (array size) is set to ten, to avoid indexing errors (index is always five)
	 * @return minimum task size of ten
	 */
	@Override
	public int GetMinimumTaskSize() 
	{
		return 10;
	}

	@Override
	public int GetMaximumTaskSize() 
	{
		return 100000000;
	}

	@Override
	public Timer GetTimer(int size) 
	{
		ArrayGenerator generator = new CleverRandomListingGenerator(size);
		return new SimpleSearcherTimer(generator.GetArray());
	}

	public static void Main(String[] args) throws IndexingError 
	{
		SimpleSearcherTimer timer = new SimpleSearcherTimer(null);
		timer.timingSequence();
	}
}
