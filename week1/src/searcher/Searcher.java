package searcher;

/**
 * A class that owns an int array, and an index.  The array may, or may not be sorted.
 *
 * Implementing classes must implement a findElement method, which will return the kth largest
 * element in the array.
 *
 * @author Hugh Osborne
 * @version September 2018
 *
 */
public abstract class Searcher 
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)
	private int[] _array; // the array in which this Searcher object will search
	private int _k; // this Searcher object searches for the kth largest entry in the array

	Searcher(int[] array, int k) 
	{
		this._array = array;
		this._k = k;
	}

	/**
	 * Get this Searcher's array
	 */
	public int[] getArray() 
	{
		return _array;
	}

	/**
	 * Get this Searcher's index
	 */
	int GetIndex()
	{ 
		return _k; 
	}

	/**
	 * Find the kth largest entry in the array
	 */
	abstract public int FindElement() throws IndexingError;
}
