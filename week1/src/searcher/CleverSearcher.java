package searcher;

import java.util.Arrays;
import java.lang.Math;

/**
 * An optimised searching algorithm for finding the nth largest item from an array.
 * 
 * @author Gemmma Mallinson
 * @version October 2018
 */
public class CleverSearcher extends Searcher
{
	CleverSearcher(int[] array, int k) 
	{
		super(array,k);
	}
	

	/**
	 * Generates an array, and partial sorts it returning the nth largest item specified by getIndex();
	 * @return The nth largest item specified by getIndex();
	 */
	@Override
	public int FindElement() throws IndexingError 
	{
		int[] array = getArray();
		int k = Math.min(GetIndex(), array.length); 		//Dont allow k to be larger than the array.
		
		int[] k_out = Arrays.copyOfRange(array, 0, k);		//Initialise the output list with the first k values of the input.
		Arrays.sort(k_out);

		for (int i = k; i < array.length; i++)				//Loop through the input list and if its a new larger value, insert it into the output in the correct position
			if (array[i] > k_out[0])
				_insertIntoSortedArray(k_out, array[i]);
		
		return k_out[0];
	}

	/**
	 * Replaces the lowest value in this sorted array with value, value is then sorted into the correct position to maintain the sort.
	 */
	private void _insertIntoSortedArray(int[] array, int value)
	{
		//Replace the lowest value in array.
		array[0] = value;
		if (array.length == 1) //If the list is only of 1 item, it is already sorted.
			return;
		
		//array is presumed to be sorted already.
		for (int i = 1; i < array.length; i++) //Search until the last value has been compared
			if (array[i-1] > array[i])
			{
				//Swap the currently considered value with the value after it, as it is larger.
				int t = array[i];	 
				array[i] = array[i-1];
				array[i-1] = t;	
			}
			else
				break; //Otherwise break as we have reached where value is supposed to be.
		
		//value is the largest in array and is at the end. 
	}	
}
