package searcher;

/**
 * Use to report index out of bounds errors
 *
 * @author Hugh Osborne
 * @version September 2016
 **/
class IndexingError extends Exception
{
	//(Code formatting changed to follow formatting used with the rest of the code, by Gemma Mallinson)
	/**
	 * Used to report an index out of bounds error
	 **/
	IndexingError() 
	{
		super("Index out of bounds");
	}
}

