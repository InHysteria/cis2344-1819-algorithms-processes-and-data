package ferrocarrilesDeAmericaDelSur.railways;

import ferrocarrilesDeAmericaDelSur.errors.RailwaySystemError;
import ferrocarrilesDeAmericaDelSur.errors.SetUpError;
import ferrocarrilesDeAmericaDelSur.tools.Clock;
import ferrocarrilesDeAmericaDelSur.tools.Delay;

/**
 * An implementation of a railway.  The runTrain method, should, in collaboration with Peru's runTrain(), guarantee
 * safe joint operation of the railways.
 */
public class Bolivia extends Railway {
	/**
     * Change the parameters of the Delay constructor in the call of the superconstructor to
	 * change the behaviour of this railway.
	 * @throws SetUpError if there is an error in setting up the delay.
	 */
	public Bolivia() throws SetUpError {
		super("Bolivia",new Delay(0.1,0.3));
	}

    /**
     * Run the train on the railway.
     * This method currently does not provide any synchronisation to avoid two trains being in the pass at the same time.
     */
	/*
    public void runTrain() throws RailwaySystemError {
    	Clock clock = getRailwaySystem().getClock();
    	while (!clock.timeOut()) {
    		choochoo();
    		crossPass();
    	}
    }
    */
    public void runTrain() throws RailwaySystemError 
    {
    	Clock clock = getRailwaySystem().getClock();
    	Railway nextRailway = getRailwaySystem().getNextRailway(this);
    	while (!clock.timeOut() && !errorOccurred()) 
    	{
    		choochoo();
    		getBasket().putStone(this);	//This train is currently crossing the passage.
    		while (nextRailway.getBasket().hasStone(this)) //While the other train is in the passage.
    		{
    			if (!getSharedBasket().hasStone(this)) //Peru has priority
    			{
	        		getBasket().takeStone(this); //This train has elected to wait and as such has rescinded its claim that it is crossing the passage.
	    			while (!getSharedBasket().hasStone(this)); //While Peru has priority.
	    			getBasket().putStone(this);	//Now the the passage is clear reinstate the claim that this train is crossing the passage.
    			}
    		}
    		
    		crossPass();
    		getBasket().takeStone(this); //This train is no longer in the passage.
    		
    		if (getSharedBasket().hasStone(this))  //If Peru doesn't have priority.
    			getSharedBasket().takeStone(this); //Shift priority to Peru.
    		
    		incrementCycles(); //Record that a cycle is completed.
    	}
    }
}