package counter;

//1, Will the test terminate?
/*
Depends on how the threads are managed, if one is given enough time to count to its limit then yes. 
With such small numbers it is likely that the test will terminate, eventually.
 */
//2, What is the shortest possible output for the test?
/*
Counter(10=[-1]=>0) has started: 10
Counter(0=[+1]=>10) has started: 0
Counter(10=[-1]=>0) has finished: 0
Counter(0=[+1]=>10) has stepped: 1
Counter(0=[+1]=>10) has stepped: 2
Counter(0=[+1]=>10) has stepped: 3
Counter(0=[+1]=>10) has stepped: 4
Counter(0=[+1]=>10) has stepped: 5
Counter(0=[+1]=>10) has stepped: 6
Counter(0=[+1]=>10) has stepped: 7
Counter(0=[+1]=>10) has stepped: 8
Counter(0=[+1]=>10) has stepped: 9
Counter(0=[+1]=>10) has stepped: 10
Counter(0=[+1]=>10) has finished: 10
 */
//3, What is the largest possible value that the count can reach when the test is run? (11)
//4, What is the lowest possible value that the count can reach when the test is run? (-1)


/**
 * A demonstration of the use and behaviour of Counters and ThreadSets.
 *
 * @author Hugh Osborne
 * @version January 2019
 */
public class Main {
    /**
     * Demonstrate the behaviour of counters and ThreadSets.  A thread set is populated with two counters, and
     * the thread set's runSet method is used to run the counters concurrently.
     *
     * @param args not used
     * @throws CounterException should not occur
     * @throws InterruptedException should not occur
     */
    public static void main(String[] args) throws CounterException, InterruptedException {
		/*
		 * Create two counters (in a thread set), and then run them with tracing on, so that their
		 * behaviour is visible.
		 */
        ThreadSet<Counter> counters = new ThreadHashSet<>();  // will contain the counters
        counters.add(new Counter(0,10,1)); // counter "up" counts from 0 to 10
        counters.add(new Counter(10,0,-1)); // counter "down" counts from 10  to 0
        Counter.traceOn(); // switch tracing on
        Counter.setDelay(0.1); // set a delay from 0.0 to 0.1 seconds
        counters.runSet(); // run the counters (concurrently)
    }
}
