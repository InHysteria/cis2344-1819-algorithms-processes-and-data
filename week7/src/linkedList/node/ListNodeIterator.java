package linkedList.node;

import java.util.Iterator;

public final class ListNodeIterator<T> implements Iterator<T> 
{
	private ListNode<T> _current;
	
	public ListNodeIterator(ListNode<T> root)
	{
		_current = root;
	}
	
	@Override
	public boolean hasNext() 
	{
		return _current.getNext() != null;
	}

	@Override
	public T next() 
	{
		T val = _current.getValue();
		_current = _current.getNext();
		
		return val;
	}

}
