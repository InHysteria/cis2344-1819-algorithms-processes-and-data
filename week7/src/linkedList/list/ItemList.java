package linkedList.list;

import java.util.Iterator;

import linkedList.node.ListNodeIterator;
import linkedList.node.SingleLinkNode;

public class ItemList<T> extends BasicList<SingleLinkNode<T>, T> implements List<T>, Iterable<T>
{
	private int _size = 0;
			
	@Override
	public void add(int index, T value) throws ListAccessError 
	{
		SingleLinkNode<T> newNode = new SingleLinkNode<T>(value);
		if (index == 0)
		{
			newNode.setNext(getRoot());
			setRoot(newNode);
		}
		else
		{
			SingleLinkNode<T> previous = getNode(index-1);	
			newNode.setNext(previous.getNext());
			previous.setNext(newNode);
		}
		
		
		_size++;
	}
	//Utility overload
	public void add(T value) throws ListAccessError
	{
		add(_size, value);
	}

	@Override
	public T remove(int index) throws ListAccessError 
	{
		SingleLinkNode<T> current = null;
		switch (_size)
		{
			case 0: throw new ListAccessError("Array is empty.");
			case 1: 
				current = getRoot();
				setRoot(null);
				break;
			default:
				SingleLinkNode<T> previous = getNode(index-1);
				current = previous.getNext();
				previous.setNext(current.getNext());
				break;
		}
		_size--;
		
		return current.getValue();
	}

	@Override
	public T get(int index) throws ListAccessError 
	{
		return getNode(index).getValue();
	}

	private SingleLinkNode<T> getNode(int index) throws ListAccessError
	{
		if (isEmpty())
			throw new ListAccessError("List is empty.");
		
		if (index < 0 || index >= _size)
			throw new ListAccessError("Index out of bounds of list.");
		
		SingleLinkNode<T> current = getRoot();
		while (index > 0)
		{
			current = current.getNext();
			index--;
		}
			
		return current;
	}
	
	public int getSize() 
	{
		return _size;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new ListNodeIterator<T>(getRoot());
	}
}
