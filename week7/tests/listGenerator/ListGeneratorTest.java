package listGenerator;

import linkedList.list.ItemList;
import linkedList.list.ListAccessError;
import listGenerator.generator.ListGenerator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Provides some basic tests for list generators:
 * <ul>
 *     <li> Do the generators generate lists of the correct size?</li>
 *     <li> Do the generators throw the expected negative array size exception if asked to generate
 *          an list of negative size?</li>
 * </ul>
 */

abstract class ListGeneratorTest<T> 
{

    abstract ListGenerator<T> getGenerator();

    /**
     * Test that a generator does produce an array of the required length.
     *
     * @param size the length of the array that should be generated
     */
    private void testSize(int size) throws ListAccessError 
    {
    	ListGenerator<T> generator = getGenerator();

    	assertEquals(size,generator.getList(size).getSize());
    }
    
    
    /**
     * Test that every item is readable in the list. Also used for testing access times.
     *
     * @param size the length of the array that should be generated
     */
    private void testRead(int size, boolean useIterator) throws ListAccessError 
    {
    	ListGenerator<T> generator = getGenerator();
    	ItemList<T> list = generator.getList(size);
    	
    	if (useIterator)
    		for (T item : list)
    			item.toString(); //Do something with item etc..
    	
    	else
    		for (int i = 0; i < size; i++)
    			list.get(i);
    }

    @Test
    void testZero() throws ListAccessError 
    {
        testSize(0);
    }

    @Test
    void testOne() throws ListAccessError 
    {
        testSize(1);
    }

    @Test
    void testTwo() throws ListAccessError 
    {
        testSize(2);
    }

    @Test
    void testTwentyThree() throws ListAccessError 
    {
        testSize(23);
    }

    @Test
    void testLarge() throws ListAccessError 
    {
        testSize(3628751);
    }

    @Test
    void testRead() throws ListAccessError 
    {
    	testRead(10000, false);
    }
    @Test
    void testReadIterator() throws ListAccessError 
    {
    	testRead(10000, true);
    }

    @Test
    void testMinusOne() 
    {
        assertThrows(NegativeArraySizeException.class, ()->testSize(-1));
    }

    @Test
    void testMinusFiftyEight() 
    {
        assertThrows(NegativeArraySizeException.class, ()->testSize(-58));
    }
}