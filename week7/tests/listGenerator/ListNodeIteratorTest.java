package listGenerator;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Iterator;

import org.junit.Test;

import arrayGenerator.scope.IntegerScope;
import linkedList.list.ItemList;
import linkedList.list.ListAccessError;
import listGenerator.generator.ScopedListGenerator;

public class ListNodeIteratorTest 
{
	@Test
	public void TestIterator() throws ListAccessError
	{
		ScopedListGenerator<Integer> generator = new ScopedListGenerator<Integer>(new IntegerScope());
		ItemList<Integer> list = generator.getList(0);
		
		for (int i=0; i<10; i++)
			list.add(i);
		
		Iterator<Integer> iterator = list.iterator();
		assertNotNull(iterator == null);
		assertTrue(iterator.hasNext());
		
		int c = 0;
		for (Integer i : list)
		{			
			if (i != c)
				fail(c + " != " + i + " at position " + c);
				
			c++;
		}
		
		if (c != 9)
			fail("Did not read entire list with iterator.");
	}
}
