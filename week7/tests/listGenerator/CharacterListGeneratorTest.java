package listGenerator;

import arrayGenerator.generator.ArrayGenerator;
import arrayGenerator.generator.CharacterArrayGenerator;
import arrayGenerator.generator.ScopedArrayGenerator;
import org.junit.jupiter.api.Test;
import arrayGenerator.scope.CharacterScope;
import arrayGenerator.scope.Scope;
import linkedList.list.ListAccessError;
import listGenerator.generator.ListGenerator;
import listGenerator.generator.ScopedListGenerator;

class CharacterListGeneratorTest extends ScopedListGeneratorTest<Character> 
{
	@Override
	ListGenerator<Character> getGenerator() {
		// TODO Auto-generated method stub
		return getGenerator(new CharacterScope());
	}
	
    @Test
    void testInRangeABC() {
        testWithinRange(new CharacterScope("ABC"),150);
    }

    @Test
    void testInSingletonRangeZ()
    {
        testWithinRange(new CharacterScope("Z"),10);
    }

    @Test
    void testInAlphabet()
    {
        testWithinRange(new CharacterScope(),150);
    }

    @Test
    void testCoversAlphabet() throws ListAccessError {
        testCoversRange(new CharacterScope());
    }

    @Test
    void testCoversSingletonQ() throws ListAccessError
    {
        testCoversRange(new CharacterScope("Q"));
    }
}