package listGenerator;

import arrayGenerator.generator.ArrayGenerator;
import arrayGenerator.generator.ScopedArrayGenerator;
import arrayGenerator.generator.StringArrayGenerator;
import org.junit.jupiter.api.Test;

import arrayGenerator.scope.CharacterScope;
import arrayGenerator.scope.Scope;
import arrayGenerator.scope.StringScope;
import linkedList.list.ListAccessError;
import listGenerator.generator.ListGenerator;

import java.util.HashSet;
import java.util.Set;

class StringListGeneratorTest extends ScopedListGeneratorTest<String> 
{
	@Override
	ListGenerator<String> getGenerator() {
		// TODO Auto-generated method stub
		return getGenerator(new StringScope());
	}

    @Test
    void testInRangeDogCatMouse() {
        Set<String> alphabet = new HashSet<String>();
        alphabet.add("Dog");
        alphabet.add("Cat");
        alphabet.add("Mouse");
        testWithinRange(new StringScope(alphabet),150);
    }

    @Test
    void testInSingletonRangeHouse()
    {
        Set<String> alphabet = new HashSet<String>();
        alphabet.add("House");
        testWithinRange(new StringScope(alphabet),10);
    }

    @Test
    void testInAlphabet()
    {
        testWithinRange(new StringScope(),150);
    }

    @Test
    void testCoversAlphabet() throws ListAccessError
    {
        testCoversRange(new StringScope());
    }

    @Test
    void testCoversSingletonHuddersfield() throws ListAccessError
    {
        Set<String> alphabet = new HashSet<String>();
        alphabet.add("Huddersfield");
        testCoversRange(new StringScope(alphabet));
    }
}