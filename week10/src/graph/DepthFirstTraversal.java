package graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;


/**
 * Extends an AjacencyGraph by providing a depth first traversal solution.
 * 
 * @author Gemma Mallinson
 * @version November 2018
 */
public class DepthFirstTraversal<T> extends AdjacencyGraph<T> implements Traversal<T> 
{

    /**
     * Perform a depth first traversal of the graph, and return the nodes in the order in which they are visited.
     *
     * @return a traversal of the graph in which each node is visited exactly once
     */
	@Override
	public List<T> traverse() throws GraphError 
	{
		HashSet<T> visited = new HashSet<T>();
		Stack<T> todo = new Stack<T>();
		List<T> out = new ArrayList<T>();
		T current;
		
		todo.addAll(getNodes()); //Start the todo list with all nodes.
		
		while (!todo.isEmpty())
		{
			current = todo.pop();
			
			if (visited.contains(current))
				continue;
			
			visited.add(current);
			out.add(current);
			todo.addAll(getNeighbours(current));
		}
		
		return out;
	}
}
