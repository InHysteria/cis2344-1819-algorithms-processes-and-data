package graph;

import org.junit.jupiter.api.Test;

import graph.DepthFirstTraversal;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepthFirstTraversalTest 
{
	@Test
	public void OrderedGraphTest10() throws GraphError
	{
		OrderedGraphTest(10);
	}
	@Test
	public void OrderedGraphTest100() throws GraphError
	{
		OrderedGraphTest(100);
	}
	@Test
	public void OrderedGraphTest1000() throws GraphError
	{
		OrderedGraphTest(1000);
	}
	
	public void OrderedGraphTest(int size) throws GraphError
	{
		DepthFirstTraversal<Integer> graph = _createOrderedGraph(size);
		
		assertTrue(graph.getNoOfNodes() == size);
		
		Integer[] traverse = graph.traverse().toArray(new Integer[size]);
		for (int i = 1; i < traverse.length; i++)
			//Is the current node one higher than the previous node in the traverse, excluding where the final node connects to the first, in which case it should wrap around.
			assertTrue(traverse[i] == (traverse[i-1]+1)%traverse.length); 
	}
	
	private DepthFirstTraversal<Integer> _createOrderedGraph(int length) throws GraphError
	{
		DepthFirstTraversal<Integer> orderedGraph = new DepthFirstTraversal<Integer>();
		
		orderedGraph.add(0);
		
		for (int i = 1; i < length; i++)
		{
			orderedGraph.add(i);
			orderedGraph.add(i-1, i);
		}
		orderedGraph.add(length-1, 0);
		
		return orderedGraph;
	}
}
