package stringSearcher;

class BoyerMooreStringSearcherTest extends StringSearcherTest {

    @Override
    StringSearcher getSearcher(String string) {
        return new BoyerMooreStringSearcher(string);
    }
}