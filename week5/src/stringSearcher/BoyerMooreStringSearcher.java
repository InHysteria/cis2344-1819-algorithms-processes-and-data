package stringSearcher;

import java.util.Hashtable;

public class BoyerMooreStringSearcher extends StringSearcher 
{
	//BadCharacterShift and GoodSuffixShift are the same, a character is just a 1 length suffix.
	private int _char_missing;
	private Hashtable<String, Integer> _segment_table;

    public BoyerMooreStringSearcher(String string) 
    {
    	this(string.toCharArray());        
    }

	public BoyerMooreStringSearcher(char[] string) 
	{
		super(string);

		_char_missing = string.length;
		_segment_table = new Hashtable<String, Integer>();
		
		String str = new String(string);
		for (int l = 1; l < string.length; l++)										//For all lengths of one or more and less than the length of the substring.
		{
			String suffix = str.substring(string.length - l, string.length);		//Get a string of the last 'l' characters in the string.
			for (int i = 0; i <= string.length - (l*2); i++)						//For every other segment of characters in the string that isn't the suffix.
				if (str.substring(i, i+l) == suffix)								//Test if this segment is equal to the suffix.
					_segment_table.put(suffix, i);									//If it is stores its position.
		}
	}

	@Override
	public int occursIn(char[] superstring) throws NotFound 
	{
		char[] substring = getString();
		String str = new String(substring);
		int shift = 0;
		
		for (int head = substring.length - 1; head >= 0; head--)
			if (shift + substring.length > superstring.length)						//If there is not enough room for the substring to exist in..
				throw new NotFound();												//It wont be found.
		
			else if (substring[head] != superstring[shift + head])					//If the character at the head in the substring does not match up with superstring
			{
				shift += _getShift(str.substring(head));							//Calculate how much to shift.
				head = substring.length - 1;										//Reset the head back to the start of the subtstring.
			}
		
		return shift;																//Return where the substring starts.
	}
	
	private int _getShift(String suffix)
	{
		int length = suffix.length();
		while (true)
			if (_segment_table.containsKey(suffix))								
				//Good Suffix Shift: Matched suffix appears elsewhere in the substring
				//Bad Character Shift: Matched character appears in the substring
				return _segment_table.get(suffix);
	
			else
				if (length == 1)
					//Bad Character Shift: Matched character does not appear in the substring
					return _char_missing;
		
				else
					//Good Suffix Shift: Matched suffix does not appear elsewhere in the substring
					suffix = suffix.substring(1);
				
	}
}
