import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class week5 
{
	private static final String filepath = "out.csv";

	public static void main(String[] args) throws FileNotFoundException
	{
		new File(filepath).delete();
		PrintWriter output = new PrintWriter("out.csv");
		CompareSorts sorts = new CompareSorts();

		System.out.println("Started...");
		sorts.Run(20, 1000000, output);
		output.flush();
		System.out.println("Done.");
	}
}
