package arraySorter;


/**
 * The implementation of selection sort.
 *
 * @author Gemma Mallinson
 * @version November 2018
 */
public class SelectionSort<T extends Comparable<? super T>> implements ArraySort<T> {

    /**
     * Sort an array.
     *
     * @param array the array to be sorted.
     * @return the sorted array.
     */
    public T[] sort(T[] array) 
    {    	
    	T tmp;
    	for (int j = 0; j < array.length - 1; j++)
    	{
    		//Find the smallest element.    		
    		int min = j;    		
    		for (int i = j+1; i < array.length; i++)	//For each unsorted index in the array, skipping the first.
    			if (array[i].compareTo(array[min]) < 0)	//If the item at the new index is smaller..
    				min = i;							//It is the new min.
    		
            tmp = array[min];							//Swap the minimum with the first unsorted item in the list.
            array[min] = array[j];
            array[j] = tmp;    			
    	}
    	
    	return array;
    }
}
