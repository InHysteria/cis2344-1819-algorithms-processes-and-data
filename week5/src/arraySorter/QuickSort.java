package arraySorter;

/**
 * The implementation of quick sort.
 *
 * @author Gemma Mallinson
 * @version November 2018
 */
public class QuickSort<T extends Comparable<? super T>> implements ArraySort<T>
{

	/**
	 * Sort an array.
	 *
	 * @param array the array to be sorted.
	 * @return the sorted array.
	 */
	public T[] sort(T[] array) 
	{
		_sort(array, 0, array.length - 1);
		return array;
	}
	
	private void _sort(T[] array, int low_bound, int high_bound)
	{
		if (low_bound >= high_bound)
			return;

		int pivot = _partition(array, low_bound, high_bound);	//Split the partition into two more ordered partitions.
		_sort(array, low_bound, pivot-1);						//Sort the smaller partition.
		_sort(array, pivot+1, high_bound);						//Sort the larger partition.
	}
	
	private int _partition(T[] array, int low_bound, int high_bound)
	{
		T pivot = array[high_bound]; 							//Pick the last element as our pivot
		int nextMin = low_bound;								//Used to establish the end of the smaller partition.
																//Everything after this index is in the larger partition.
		
		for (int i = low_bound; i < high_bound; i++)			//For all elements not including the pivot
			if (array[i].compareTo(pivot) <= 0)					//If the item at i is smaller than or equal to the pivot
			{
				_swap(array, i, nextMin);						//It should be placed before pivot.
				nextMin++;										//The smaller partition has grown by 1.
			}
		
		_swap(array, nextMin, high_bound);						//Pivot is placed at the end of the smaller partition.
																//This makes pivot exit between the smaller and larger partition.
		
		return nextMin;											//Return the index that pivot is at.
	}
	
	private void _swap(T[] array, int a, int b)
	{
		if (a == b) return;
		
		T tmp = array[a];
		array[a] = array[b];
		array[b] = tmp;			
	}
}
