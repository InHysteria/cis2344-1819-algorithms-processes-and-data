

import arrayGenerator.IntegerArrayGenerator;
import arraySorter.ArraySort;
import arraySorter.BubbleSort;
import arraySorter.QuickSort;
import arraySorter.SelectionSort;
import scope.IntegerScope;

import java.io.PrintWriter;
import java.time.Duration;
import java.util.ArrayList;

public class CompareSorts 
{
	private final IntegerArrayGenerator _generator = new IntegerArrayGenerator(new IntegerScope());
	private ArrayList<ArraySort<Integer>> _sorters = new ArrayList<ArraySort<Integer>>();
	
	public CompareSorts()
	{
		_sorters.add(new QuickSort<Integer>());
		//_sorters.add(new SelectionSort<Integer>());
		//_sorters.add(new BubbleSort<Integer>());
	}
	
	public void Run(int n, int interval, PrintWriter output)
	{
		long startTime, lastCycleStart, sum, size;
		Integer[] array, arrayClone;
		

		output.write("ArraySize,");
		output.write("O(n^2)");
		for (ArraySort<Integer> sorter : _sorters)
	        output.write(sorter.getClass().getName() + ",");
		output.write("\n");
		
		
		for (int i = 1; i <= n; i++)
		{
			size = i * interval;
			array = _generator.getArray(i * interval);

			lastCycleStart = Duration.ofNanos(System.nanoTime()).toMillis(); 
			output.write(size + ",,");

			for (ArraySort<Integer> sorter : _sorters)
			{
				sum = 0;
				for (int x = 0; x < 5; x++)
				{
					arrayClone = array.clone();
			        startTime = Duration.ofNanos(System.nanoTime()).toMillis();
			        sorter.sort(arrayClone);
			        sum += Duration.ofNanos(System.nanoTime()).toMillis() - startTime;
				}
		        
		        output.write((sum/5) + ",");
		        
			}

			output.write("\n");
			System.out.println("["+(Duration.ofNanos(System.nanoTime()).toMillis() - lastCycleStart)+" ms]: Test "+i+", size "+size+"");
		}			
	}
}
