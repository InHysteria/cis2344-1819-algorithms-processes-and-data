package binaryTree;

import java.util.Random;

public class CharacterBinaryTreeTest extends BinaryTreeTest<Character> 
{
	private static Random _random = new Random();
		
	@Override
	public Character generateItem() 
	{
		return (char)(65 +_random.nextInt(26) + (_random.nextInt(2) * 32));
	}
}
