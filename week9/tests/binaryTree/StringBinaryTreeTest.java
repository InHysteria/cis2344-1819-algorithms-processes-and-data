package binaryTree;

import java.util.Random;

public class StringBinaryTreeTest extends BinaryTreeTest<String> 
{
	private static Random _random = new Random();
	private static final int _MIN_LEN = 2;
	private static final int _MAX_LEN = 100;
		
	@Override
	public String generateItem() 
	{
		StringBuilder builder = new StringBuilder();
		
		int len = _random.nextInt(_MAX_LEN-_MIN_LEN)+_MIN_LEN;
		while (len-- > 0)
			builder.append((char)(65 +_random.nextInt(26) + (_random.nextInt(2) * 32)));
		
		return builder.toString();
	}
}
