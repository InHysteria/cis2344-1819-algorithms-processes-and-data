package binaryTree;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

abstract class BinaryTreeTest<T extends Comparable<? super T>> 
{
	@Test
	public void Test10()
	{
		_testContents(10);
	}
	
	@Test
	public void Test100()
	{
		_testContents(100);
	}

	@Test
	public void Test2()
	{
		_testContents(2);
	}

    @Test
    public void testMinusOne() 
    {
        assertThrows(NegativeArraySizeException.class, ()->_testContents(-1));
    }

    @Test
    public void testMinusFiftyEight() 
    {
        assertThrows(NegativeArraySizeException.class, ()->_testContents(-58));
    }
	
	private void _testContents(int size)
	{
		ArrayList<T> sample = _generateData(size);
		BinaryTree<T> tree = new BinaryTree<T>();	
		
		for (T i : sample)
			tree.insert(i);		

		List<T> out = tree.traverse();		
		//Test is all items in traversal are monotonic
		
		T l = null;
		int direction = 0;
		for (T t : out)
			if (l == null)
				l = t;
			
			else if (direction == 0)
				direction = l.compareTo(t);
			
			else
				assertTrue(
				 (l.compareTo(t) > 0 && direction > 0)
			  || (l.compareTo(t) < 0 && direction < 0));
		
		//Test if all items added to tree exist in traversal.
		for (T i : sample)
		{
			boolean found = false;
			for (T t : out)
				if ((found = i == t))
					break;
			
			if (!found)
				fail(i + " is missing from tree after traversal.");
		}
	}
	
	private ArrayList<T> _generateData(int size)
	{
		if (size < 0)
			throw new NegativeArraySizeException();
		
		ArrayList<T> data = new ArrayList<T>();
		for (int i = 0; i < size; i++)
			data.add(generateItem());
		
		return data;			
	}

	public abstract T generateItem();
}

