package binaryTree;

@SuppressWarnings("serial")
public class EmptyTreeException extends Exception {
	public EmptyTreeException()
	{
		super("The current node is empty.");
	}
}
