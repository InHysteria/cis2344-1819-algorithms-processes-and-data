package semaphore;

import java.lang.Math;

public class AbsorbingSemaphore implements SemaphoreInterface
{
	private String _name;
	private int _value;
	private int _maxValue;
	
	public AbsorbingSemaphore(String name,int initialValue,int maxValue)
	{
		_name = name;
		_value = initialValue;
		_maxValue = maxValue;
	}
	
	public synchronized void poll() throws InterruptedException
	{
		if (--_value < 0)
			wait();
	}
	public synchronized void vote() throws InterruptedException
	{
		_value = Math.min(_maxValue, _value+1);
		if (_value <= 0)
			notify();
	}
	
	@Override
	public String getName() 
	{
		return _name;
	}	
}
