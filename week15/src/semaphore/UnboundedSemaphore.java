package semaphore;

public class UnboundedSemaphore implements SemaphoreInterface
{
	private String _name;
	private int _value;
	
	public UnboundedSemaphore(String name,int initialValue)
	{
		_name = name;
		_value = initialValue;
	}
	
	public synchronized void poll() throws InterruptedException
	{
		if (--_value < 0)
			wait();
	}
	public synchronized void vote() throws InterruptedException
	{
		if (++_value <= 0)
			notify();
	}
	
	@Override
	public String getName() 
	{
		return _name;
	}	
}
