package arithmetic;

import javax.swing.JOptionPane;

public class Cube {
	public static int cube(int n) {
		  int cube = 0, threeNsquared = 0, threeN = 0;

		  int i = 0;
		  while (i < n) {
			  cube = cube + threeNsquared + threeN + 1;
			  threeNsquared = threeNsquared + 2*threeN + 3;
			  threeN = threeN + 3;
			  i++;
		  }
		  return cube;
	}
	
	public static void main(String[] args) {
		int x = ReadInt.read("Please input a number to be squared");
		int result = cube(x);
		JOptionPane.showMessageDialog(null,x + "^3=" + result,"Function result",JOptionPane.INFORMATION_MESSAGE);
	}

}
