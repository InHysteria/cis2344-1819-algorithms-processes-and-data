package genericMethods;

import org.junit.jupiter.api.Test;

import unaryPredicate.IsEven;

import static org.junit.jupiter.api.Assertions.*;


class ArrayUtilsTest 
{

private Integer[] _create_sample() 
{
	return new Integer[] { 16, 9, 3, 10, 20, 14, 12, 7, 19, 8, 11, 17, 6, 2, 5, 18, 1, 13, 15, 4 };
}

@Test
public void TestSwap() 
{
	Integer[] array = _create_sample();							//Generate a standard sample array.
	int index_a = (int)(Math.random() * array.length);			//Pick a random index from the array.
	int index_b = (int)(Math.random() * (array.length - 1));	//Pick another random index from the array.
	
	if (index_b >= index_a)
		index_b = index_b + 1;									//Prevent the same index from being chosen for both indicies.
	
	int expected_a = array[index_b];							//Item at index_a should be the item at index_b after operation.
	int expected_b = array[index_a];							//Item at index_b should be the item at index_a after operation.
	
	ArrayUtils.Swap(array, index_a, index_b);					//Swap the two items.
	assertTrue(array[index_a] == expected_a && array[index_b] == expected_b);	//Test if the predictions were correct.
}
	
	@Test
	public void TestMaxAll()
	{
		Integer[] array = _create_sample();
		int index_a = 0;
		int index_b = array.length - 1;
		int expected = 20;
		
		assertTrue(_testMax(array, index_a, index_b) == expected);
	}
	
	@Test
	public void TestMaxSubset()
	{
		Integer[] array = _create_sample();
		int index_a = 0;
		int index_b = 3;
		int expected = 16;
		
		assertTrue(_testMax(array, index_a, index_b) == expected);
	}
	
	@Test
	public void TestSatisfies()
	{
		Integer[] array = _create_sample();
		IsEven predicate = new IsEven();
		
		assertTrue(ArrayUtils.Satisfies(array, predicate) == 10);
	}
	
	private int _testMax(Integer[] array, int index_a, int index_b) 
	{
		return ArrayUtils.Max(array, index_a, index_b);
	}
}
