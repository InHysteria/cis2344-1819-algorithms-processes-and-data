package genericMethods;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class EqualsTest 
{

    /**
     * A selection of tests for equals
     */

    @Test
    public void TestEqualIntegers() 
    {
        assertTrue(Equals.equals(3, 3));
    }

    @Test
    public void TestNotEqualIntegers() 
    {
        assertFalse(Equals.equals(3, 7));
    }

    @Test
    public void TestEqualStrings() 
    {
        assertTrue(Equals.equals("Hugh", "Hugh"));
    }

    @Test
    public void TestNotEqualStrings() 
    {
        assertFalse(Equals.equals("Hugh", "hugh"));
    }

    @Test
    public void TestEqualNull() 
    {
        assertTrue(Equals.equals(null, null));
    }

    @Test
    public void TestNotEqualNull1()
    {
        assertFalse(Equals.equals(null, 4));
    }

    @Test
    public void TestNotEqualNull2() 
    {
        assertFalse(Equals.equals("Hugh", null));
    }    
}