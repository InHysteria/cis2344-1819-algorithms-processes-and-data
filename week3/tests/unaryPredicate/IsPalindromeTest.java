package unaryPredicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsPalindromeTest {

    private IsPalindrome _predicate = new IsPalindrome();

    @Test
    public void TestRacecar()
    {
    	assertTrue(_predicate.Test("racecar"));
    }
    
    @Test
    public void TestToyota()
    {
    	assertTrue(_predicate.Test("A toyota's a toyota"));
    }
    
    @Test
    public void TestRaceFast()
    {
    	assertTrue(_predicate.Test("Race fast safe car!"));
    }
    
    @Test
    public void TestHello()
    {
    	assertFalse(_predicate.Test("Hello World!"));
    }
    
    @Test
    public void TestFale()
    {
    	assertFalse(_predicate.Test("Fale"));
    }
    
    @Test
    public void TestMorgor()
    {
    	assertFalse(_predicate.Test("Morgor, destroyer of worlds."));
    }   
}