package unaryPredicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsEvenTest 
{
    private IsEven predicate = new IsEven();

    @Test
    public void TestZero()
    {
        assertTrue(predicate.Test(0));
    }

    @Test
    public void TestOne()
    {
        assertFalse(predicate.Test(1));
    }

    @Test
    public void TestTwo()
    {
        assertTrue(predicate.Test(2));
    }

    @Test
    public void TestThree() 
    {
        assertFalse(predicate.Test(3));
    }

    @Test
    public void TestBigEven()
    {
        assertTrue(predicate.Test(2*((Integer.MAX_VALUE-1)/2)));
    }

    @Test
    public void TestBigOdd()
    {
        assertFalse(predicate.Test(2*((Integer.MAX_VALUE-1)/2)-1));
    }

    @Test
    public void TestMinusOne() 
    {
        assertFalse(predicate.Test(-1));
    }

    @Test
    public void TestMinusTwo() 
    {
        assertTrue(predicate.Test(-2));
    }

    @Test
    public void TestMinusThree() 
    {
        assertFalse(predicate.Test(-3));
    }

    @Test
    public void TestMinusBigEven() 
    {
        assertTrue(predicate.Test(2*((Integer.MIN_VALUE+1)/2)));
    }

    @Test
    public void TestMinusBigOdd() 
    {
        assertFalse(predicate.Test(2*((Integer.MIN_VALUE+1)/2)+1));
    }
}