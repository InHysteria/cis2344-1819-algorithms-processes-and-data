package unaryPredicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsPrimeSieveTest 
{
    private IsPrimeSieve _predicate = new IsPrimeSieve();

    @Test
    public void TestSmallPrimes() 
    {
        assertTrue(_predicate.Test(1));
        assertTrue(_predicate.Test(2));
        assertTrue(_predicate.Test(3));
        assertTrue(_predicate.Test(5));
        assertTrue(_predicate.Test(7));
        assertTrue(_predicate.Test(11));
        assertTrue(_predicate.Test(13));
        assertTrue(_predicate.Test(17));
    }
    
    @Test
    public void TestBigPrimes() 
    {
        assertTrue(_predicate.Test(7727));
        assertTrue(_predicate.Test(7741));
        assertTrue(_predicate.Test(7753));
        assertTrue(_predicate.Test(7757));
        assertTrue(_predicate.Test(7759));
        assertTrue(_predicate.Test(7789));
        assertTrue(_predicate.Test(7793));
        assertTrue(_predicate.Test(7817));
    	assertTrue(_predicate.Test(Integer.MAX_VALUE));
    }
    
    @Test
    public void TestNotPrimes() 
    {
    	assertFalse(_predicate.Test(10));
    	assertFalse(_predicate.Test(90));
    	assertFalse(_predicate.Test(60605201));
    }
}