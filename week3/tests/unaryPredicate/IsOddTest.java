package unaryPredicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsOddTest 
{
    private IsOdd _predicate = new IsOdd();

    @Test
    public void TestZero() 
   	{
        assertFalse(_predicate.Test(0));
    }

    @Test
    public void TestOne() 
    {
        assertTrue(_predicate.Test(1));
    }

    @Test
    public void TestTwo() 
    {
    	assertFalse(_predicate.Test(2));
    }

    @Test
    public void TestThree() 
    {
    	assertTrue(_predicate.Test(3));
    }

    @Test
    public void TestBigEven() 
    {
    	assertFalse(_predicate.Test(2*((Integer.MAX_VALUE-1)/2)));
    }

    @Test
    public void TestBigOdd() 
    {
    	assertTrue(_predicate.Test(2*((Integer.MAX_VALUE-1)/2)-1));
    }

    @Test
    public void TestMinusOne() 
    {
	   assertTrue(_predicate.Test(-1));
    }

    @Test
    public void TestMinusTwo() 
    {
    	assertFalse(_predicate.Test(-2));
    }

    @Test
    public void TestMinusThree() 
    {
    	assertTrue(_predicate.Test(-3));
    }

    @Test
    public void TestMinusBigEven() 
    {
        assertFalse(_predicate.Test(2*((Integer.MIN_VALUE+1)/2)));
    }

    @Test
    public void TestMinusBigOdd() 
    {
    	assertTrue(_predicate.Test(2*((Integer.MIN_VALUE+1)/2)+1));
    }
}