package unaryPredicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsMonotonicTest 
{

    private IsMonotonic<Integer> _predicateInt = new IsMonotonic<Integer>();
    private IsMonotonic<Float> _predicateFloat = new IsMonotonic<Float>();

    @Test
    public void TestIntegerIncreasing1()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
    }
    
    @Test
    public void TestIntegerIncreasing5()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45 }));
    }
    
    @Test
    public void TestIntegerDecreasing1()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }));
    }
    
    @Test
    public void TestIntegerDecreasing5()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 45, 40, 35, 30, 25, 20, 15, 10, 5, 0 }));
    }
    
    @Test
    public void TestIntegerFail()
    {
    	assertFalse(_predicateInt.Test(new Integer[] { 10, 0, -10, 0, 10 } ));
    }
    
    @Test
    public void TestFloatIncreasing1()
    {
    	assertTrue(_predicateFloat.Test(new Float[] { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f }));
    }
    
    @Test
    public void TestFloatDecreasing1()
    {
    	assertTrue(_predicateFloat.Test(new Float[] { 9f, 8f, 7f, 6f, 5f, 4f, 3f, 2f, 1f, 0f }));
    }
    
    @Test
    public void TestFloatFail()
    {
    	assertFalse(_predicateFloat.Test(new Float[] { 1f, 0f, -1f, 0f, 1f }));
    }

    @Test
    public void TestIntegerIncreasingLeading0()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 0, 0, 0, 0, 0, 1, 1, 4, 6, 7, 7, 7, 7, 7, 20 }));
    }
    
    @Test
    public void TestIntegerDecreasingLeading0()
    {
    	assertTrue(_predicateInt.Test(new Integer[] { 0, 0, 0, 0, 0, -1, -1, -4, -6, -7, -7, -7, -7, -7, -20 }));
    }
}