package unaryPredicate;

import java.util.ArrayList;

/**
 * An implementation of 
 * 
 * @author Gemmma Mallinson
 * @version October 2018
 */

public class IsPrimeSieve extends CountingUnaryPredicate<Integer> 
{

    /**
     * Test whether a number is prime.
     *
     * @param n the number t be tested
     * @return true if n is prime
     */
    @Override
    public boolean Test(Integer n) 
    {
    	ArrayList<Integer> primes = new ArrayList<Integer>();
    	Double sqrt = Math.sqrt(n);
    	for (Integer i = 2; i < sqrt; i++)
    		if (_isPrime(primes, i))
    		{
    			primes.add(i);
    			if (n % i == 0)
    				return false;
    		}
    				
    	return true;
    }
    
    private boolean _isPrime(ArrayList<Integer> primes, Integer i)
    {
		for (Integer prime : primes)
			if (i % prime == 0)
				return false;
		
		return true;
    }
}
