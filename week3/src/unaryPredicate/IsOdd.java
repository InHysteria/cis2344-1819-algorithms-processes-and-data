package unaryPredicate;

/**
 * A predicate for classifying odd integers.
 *
 * @author Gemmma Mallinson
 * @version October 2018
 */
public class IsOdd extends CountingUnaryPredicate<Integer> {

    /**
     * Test whether a number is odd.
     *
     * @param n the number t be tested
     * @return true if n is odd
     */
    @Override
    public boolean Test(Integer n) {
        return n % 2 != 0;
    }
}
