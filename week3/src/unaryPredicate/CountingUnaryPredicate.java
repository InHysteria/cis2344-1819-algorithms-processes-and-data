package unaryPredicate;

import genericMethods.ArrayUtils;

/**
 * 
 * An implementation of UnaryPredicate which utilises ArrayUtils.Satisfies
 * to provide a count of which pass the implementing predicate.
 *
 * @author Gemma Mallinson
 * @version October 2018
 */

public abstract class CountingUnaryPredicate<T> implements UnaryPredicateCount<T> 
{
	@Override
    public int NumberSatisfying(T[] array)
    {
		return ArrayUtils.Satisfies(array, this);
    }
}
