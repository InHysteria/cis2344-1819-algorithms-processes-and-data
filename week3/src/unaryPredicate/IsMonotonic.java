package unaryPredicate;

/**
 * A predicte which classifies arrays which maintain a consistent direction across all their items.
 *
 * @author Gemma Mallinson
 * @version October 2018
 */

public class IsMonotonic<T extends Comparable<T>> extends CountingUnaryPredicate<T[]> 
{

    /**
     * Test whether an array maintains a consistent direction.
     *
     * @param n the array to be tested
     * @return true if the array is monotonic
     */
    @Override
    public boolean Test(T[] n) 
    {
    	if (n.length < 3) 								//A 1 or 0 length array has no direction. A 2 length array can never change its direction.
    		return true;
    	
    	int direction = n[0].compareTo(n[1]);			//Establish the direction of the array by comparing the first two items.
    	for (int i = 1; i < n.length-1; i++)			//For every item in the array after the first and not including the last.
    	{
    		int step = n[i].compareTo(n[i+1]); 
    		if (step == 0)								//If the item is the same as the previous item, move onto the next item pair.
    			continue;
    		
    		if (direction == 0)							//If this is the first pair to establish a direction, initialise this lists direction to this.
    			direction = step;
    		else if  ( step < 0 && direction > 0			//If the step and direction are on opposite sides of 0.
    				|| step > 0 && direction < 0)
    			return false;							//The list is not monotonic.
    		
    	}
    	
    	return true;									//The array is monotonic.    			
    }
}
