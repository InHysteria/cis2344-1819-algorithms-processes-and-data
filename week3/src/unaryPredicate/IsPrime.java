package unaryPredicate;

/**
 * A predicate for classifying prime numbers.
 * 
 * @author Gemmma Mallinson
 * @version October 2018
 */

public class IsPrime extends CountingUnaryPredicate<Integer> 
{

    /**
     * Test whether a number is prime.
     *
     * @param n the number t be tested
     * @return true if n is prime
     */
    @Override
    public boolean Test(Integer n) 
    {
    	if (n == 2)
    		return true;  					//Two is the only even prime.
    	
    	if (n % 2 == 0)
    		return false; 					//There are no other even primes.
    		
    	Double sqrt = Math.sqrt(n);
    	for (int i = 3; i <= sqrt; i+=2)	//Check every number lower than the square root of n.
    		if (n % i == 0)					//If n is cleanly divisible by i.
				return false;
    	
    	return true;    	
    }
}
