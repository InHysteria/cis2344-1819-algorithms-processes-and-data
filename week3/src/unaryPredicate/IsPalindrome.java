package unaryPredicate;

/**
 * A predicate for classifying strings which are palindromes.
 *
 * @author Gemmma Mallinson
 * @version October 2018
 */

public class IsPalindrome extends CountingUnaryPredicate<String>
{
    /**
     * Test whether string n is a palindrome
     *
     * @param n the string to  be tested.
     * @return true iff n is even
     */
    @Override
    public boolean Test(String n) 
    {
    	String p = n
			.toLowerCase()									//Convert the string to lower case as 'A' != 'a'
			.replaceAll("(\\s|[',\\.\\-!\\?])", ""); 		//Remove all spaces and grammatical marks from the word.
    	
    	int length = p.length();							//Store how long the string is for ease of access.
    	for (int i = 0; i < length/2; i++)					//For half of the string (We are comparing two characters at the same time so no need to check each char twice)
    		if (p.charAt(i) != p.charAt(length-i-1))		//If the ith position from the start is not the same as the ith position from the end.
    			return false;								//	the string is not a palindrome.
    	
    	return true;										//The string is a palindrome.
    }
}
