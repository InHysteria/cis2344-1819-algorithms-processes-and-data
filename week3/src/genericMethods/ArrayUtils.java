package genericMethods;

import unaryPredicate.UnaryPredicate;

/**
 * A series of utilities for operating upon an array.
 * 
 * @author Gemmma Mallinson
 * @version October 2018
 */
public class ArrayUtils 
{
/**
 * For a given array, swaps the items at index_a and index_b with each other.
 *
 * @param array the array to perform the swap upon.
 * @param index_a the first index to swap. Item at index_a will end up at index_b.
 * @param index_b the second index to swap. Item at index_b will end up at index_a.
 * @return the array passed in the array parameter.
 */
public static <T> T[] Swap(T[] array, int index_a, int index_b)
{			
	T t = array[index_b];						//Temporarily store the item at index_b.		
	array[index_b] = array[index_a];			//Set index_b to the item at index_a		
	array[index_a] = t;							//Set index_a to the item that was at index_b		
	
	return array;								//Return the array so that additional calls may be made on it.
}

	/**
	 * For a given array, finds the largest item between index_a and index_b. 
	 * 
	 * @param array the array to search for the largest item in.
	 * @param index_a the index to start searching at.
	 * @param index_b the index, which when reached, will finish the search. This index is included in the search.
	 * @return the array passed in the array parameter.
	 */
	public static <T extends Comparable<T>> T Max(T[] array, int index_a, int index_b)
	{	
		T max = array[index_a]; 					//Assume the first value is the max initially.
		
		for (int i = index_a+1; i <= index_b; i++)	//Go through all elements after index_a up to, and including, index_b.
			if (array[i].compareTo(max) > 0)		//If the current item being considered is larger than the current max.
				max = array[i];						//Update the current max to this new item.
		
		return max;									//Return the highest value found.
	}
	
	/**
	 * For a given array, returns how  many items pass a given predicate. 
	 * 
	 * @param array the array to count items from.
	 * @param predicate the predicate to classify items from the array.
	 * @return the array passed in the array parameter.
	 */	
	public static <T> int Satisfies(T[] array, UnaryPredicate<T> predicate)
	{
		int count = 0;								//Initialise the count as 0, there are currently no items in the array that have passed the predicate.
		
		for (T item : array)						//Go through every item in the array.
			if (predicate.Test(item))				//If the predicate returns true for this item,
				count++;							//	increase the count;
		
		return count;								//Return how many items have passed the predicate.
	}
}
